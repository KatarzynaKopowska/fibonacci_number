
fib_num = [0,1]

for i in range(2, 10):  
    fib_num.append(fib_num[-1] + fib_num[-2]) 

print('\n'.join(str(i) for i in fib_num))
